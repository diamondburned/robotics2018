#pragma config(Sensor,  dgtl1,   rightEncoder,  sensorQuadEncoder)
#pragma config(Sensor,  dgtl3,   leftEncoder,   sensorQuadEncoder)
#pragma config(Motor,   port1,   rightMotor,	tmotorNormal,   openLoop,   reversed)
#pragma config(Motor,   port10,  leftMotor,	    tmotorNormal,   openLoop)

// bothEncoders should be used in a while loop to check for both sensors at once
bool bothEncoders(int degrees) {
	return SensorValue[rightEncoder] >= degrees || SensorValue[leftMotor] >= degrees;
}

// resetEncoders resets both encoders to 0
void resetEncoders() {
	SensorValue[rightEncoder] = 0;
	SensorValue[leftMotor]    = 0;
	return;
}

// setMotor rightMotor leftMotor
void setMotor(int Rspeed, int Lspeed) {
	motor[rightMotor] = Rspeed;
	motor[leftMotor]  = Lspeed;
	return;
}

// For 1000 degrees experimentally, we get 27.5 inches
// ToShaft converts from inches to degrees
int ToShaft(int inches) {
	//	 v degrees 
	return 1000 / 27.5 * inches;
}

// DegreesTurnCalc calculates degrees with the engine speed of 96
int DegreesTurnCalc(float degrees) {
	return round(3.273070 * degrees + 0.700565);
}

void TurnDegreesCW(int degrees) {
	int distance = DegreesTurnCalc(degrees);
	writeDebugStreamLine("Turning %d degrees or moving %d forward/backward speed of 96 CLOCKWISE.", degrees, distance);
	while (bothEncoders(distance)) {
		setMotor(96, -96);
	}

	setMotor(0, 0);
	return;
}

void TurnDegreesAW(int degrees) {
	int distance = DegreesTurnCalc(degrees);
	writeDebugStreamLine("Turning %d degrees or moving %d forward/backward speed of 96 ANTI-CLOCKWISE.", degrees, distance);
	while (bothEncoders(distance)) {
		setMotor(-96, 96);
	}

	setMotor(0, 0);
	return;
}

task main() {
	sleep(2000);
  //motor[rightMotor] = 100;
	writeDebugStreamLine("Moving 31in forward");
	while (bothEncoders(ToShaft(31))) {
		writeDebugStreamLine("%v", bothEncoders(ToShaft(31)));
		setMotor(100, 100);
	}

	resetEncoders();

	// Turn left 90 degrees
	TurnDegreesCW(90);

	writeDebugStreamLine("Moving 34in forward");
	while (bothEncoders(ToShaft(34))) {
		setMotor(100, 100);
	}

	resetEncoders();

	// Turn right 90 degrees
	TurnDegreesAW(90);

	writeDebugStreamLine("Moving 20 degrees forward");
	while (bothEncoders(ToShaft(20))) {
		setMotor(100, 100);
	}

	resetEncoders();

	TurnDegreesAW(90);

	writeDebugStreamLine("Moving 15 degrees forward");
	while (bothEncoders(ToShaft(15))) {
		setMotor(100, 100);
	}
}